import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import {
    Grid,
    Header,
} from 'semantic-ui-react';
import { useLocation } from "react-router-dom";

import OwnedPokemonCard from '../components/card/OwnedPokemonCard';

const CapturedPokemon = () => {
    let location = useLocation();

    const [capturedPokemons, setCapturedPokemons] = useState([]);

    return (
    <>
        <Helmet>
            <title>{"Captured Pokemons - " + process.env.REACT_APP_NAME}</title>
            <meta name="description" content={"Captured Pokemons - " + process.env.REACT_APP_NAME} />
        </Helmet>
        <Header as="h2" color="orange" textAlign="center">Captured Pokemons</Header>
        <Grid columns={4} doubling>
            {
                capturedPokemons.length ? 
                    capturedPokemons.map((pokemon) => {
                        return (
                            <OwnedPokemonCard index={pokemon.id} name={pokemon.name} sprites={capturedPokemons} detailUrl={`/pokemon-detail/${pokemon.id}`} />
                        )
                    }) : ('')
            }
        </Grid>
    </>
    );
};

export default React.memo(CapturedPokemon);