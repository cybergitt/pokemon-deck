// Unused due to blocked cors origin from api server to localhost
import { gql } from '@apollo/client';

export const GET_POKEMONS = gql`
    query pokemons($limit: Int, $offset: Int) {
        pokemons(limit: $limit, offset: $offset) {
            count
            next
            previous
            status
            message
            results {
                url
                name
                image
            }
        }
    }
`;

export const GET_POKEMONS_VARS = {
  limit: 2,
  offset: 1,
};