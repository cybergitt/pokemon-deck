import { lazy } from 'react';

const routes = [
    {
        path: '/',
        exact: true,
        Component: lazy(() => import('../scenes/WildPokemon'))
    },
    {
        path: '/pokemon-detail/:id',
        exact: true,
        Component: lazy(() => import('../scenes/PokemonDetail'))
    },
    {
        path: '/captured-pokemons',
        exact: true,
        Component: lazy(() => import('../scenes/CapturedPokemon'))
    },
];

export default routes;